/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_bits.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 20:33:22 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 20:40:27 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

unsigned char rev_bits(unsigned char b)
{
	unsigned char r = 0;
	unsigned byte_len = 8;
	while (byte_len--)
	{
		r = (r << 1) | (b & 1);
		b = b >> 1;
	}
	return (r);
}

int main(int ac, char **av)
{
	printf("%d", rev_bits(atoi(av[1])));
	return (0);
}
