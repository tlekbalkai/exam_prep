/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat_alph.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 15:38:03 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 15:47:51 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		num(char a)
{
	int res;
	if (a >= 'a' && a <= 'z')
		return (a - 'a' + 1);
	if (a >= 'A' && a <= 'Z')
		return (a - 'A' + 1);
	return (-1);
}

void	rep_alph(char *str)
{
	int i;
	int j;

	i = 0;
	while (str[i])
	{
		j = 0;
		if (num(str[i]) == -1)
			ft_putchar(str[i]);
		else
			while (j < num(str[i]))
			{
				ft_putchar(str[i]);
				j++;
			}
		i++;
	}
}

int	main(int ac, char **av)
{
	if (ac == 2)
	{
		rep_alph(av[1]);
	}
	ft_putchar('\n');
	return (0);
}
