/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 19:55:53 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 20:01:46 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#define abs(x) (x < 0) ? -x : x

void aux(int num, char *res, int *i)
{
	char signs[] = "0123456789";
	if (num <= -10 || num >= 10)
		aux(num / 10, res, i);
	res[(*i)++] = signs[abs(num % 10)];
}

char *ft_itoa(int nb)
{
	char *res;
	int i = 0;

	res = malloc(sizeof(char) * 35);
	if (!res)
		return (0);
	if (nb < 0)
	{
		res[i++] = '-';
		nb = -nb;
	}
	aux(nb, res, &i);
	res[i] = '\0';
	return (res);
}

int main(int ac, char **av)
{
	printf("%s\n", ft_itoa(atoi(av[1])));
}
