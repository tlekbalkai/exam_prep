/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inter1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 20:01:17 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 20:07:01 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

int check(char c, char *str, int i)
{
	int j = 0;
	while (j < i)
	{
		if (str[j] == c)
			return (0);
		j++;
	}
	return (1);
}

void inter(char *str1, char *str2)
{
	int i = 0;
	int j = 0;

	while (str1[i])
	{
		j = 0;
		if (check(str1[i], str1, i) == 1)
		{
			while (str2[j])
			{
				if (str1[i] == str2[j])
				{
					ft_putchar(str1[i]);
					break ;
				}
				j++;
			}
		}
		i++;
	}
}


int main(int ac, char **av)
{
	if (ac == 3)
		inter(av[1], av[2]);
	ft_putchar('\n');
	return (0);
}
