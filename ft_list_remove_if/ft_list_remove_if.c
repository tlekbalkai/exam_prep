/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 20:10:47 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 20:19:00 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

typedef struct	s_list
{
	struct s_list	*next;
	void			*data;
}				t_list;

void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)())
{
	t_list *list;
	t_list *temp;

	list = *begin_list;
	while (list->next)
	{
		if (cmp(list->next->data, data_ref) == 0)
		{
			temp = list->next;
			list->next = temp->next;
			free(temp);
		}
		else
			list = list->next;
	}
	if (cmp((*begin_list)->data, data_ref) == 0)
	{
		temp = (*begin_list);
		*begin_list = temp->next;
		free(temp);
	}
}
