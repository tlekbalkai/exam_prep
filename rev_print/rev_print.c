/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_print.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 15:50:27 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 15:58:01 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	rev_str(char *str)
{
	int i;

	i = ft_strlen(str);
	while (i >= 0)
	{
		ft_putchar(str[i]);
		i--;
	}
}

int	main(int ac, char **av)
{
	if (ac == 2)
		rev_str(av[1]);
	ft_putchar('\n');
	return (0);
}
