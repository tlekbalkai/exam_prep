/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alpha_mir.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 16:33:49 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 16:40:10 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	lower(char c)
{
	c = c - 'a';
	c = 25 - c;
	c = c + 'a';
	ft_putchar(c);
}

void	upper(char c)
{
    c = c - 'A';
    c = 25 - c;
    c = c + 'Z';
	ft_putchar(c);
}

void	alpha_mir(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			lower(str[i]);
		else if (str[i] >= 'A' && str[i] <= 'Z')
			upper(str[i]);
		else
			ft_putchar(str[i]);
		i++;
	}
}

int		main(int ac, char **av)
{
	if (ac == 2)
	{
		alpha_mir(av[1]);
	}
	ft_putchar('\n');
	return (0);
}
