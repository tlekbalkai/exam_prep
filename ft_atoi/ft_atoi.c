/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 17:03:51 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 17:11:22 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_atoi(char *c)
{
	int i;
	int res;
	int n;

	i = 0;
	res = 1;
	n = 0;
	while (c[i] <= 32)
		i++;
	if (c[i] == '-')
	{
		res = -1;
		i++;
	}
	while (c[i] == '+' || c[i] == '-')
		i++;
	while (c[i] >= '0' && c[i] <= '9')
	{
		n = n * 10 + (c[i] - '0');
		i++;
	}
	return (res * n);
}

int main(int ac, char **av)
{
	printf("%d\n", ft_atoi(av[1]));
	return (0);
}
