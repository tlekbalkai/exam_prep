/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 19:21:26 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 19:37:37 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

void	print_hex(int n)
{
	if (n >= 16)
		print_hex(n / 16);
	n = n % 16;
	if (n < 10)
		n += 48;
	else
		n += 'a' - 10;
	printf("%c", n);
}

int main(int ac, char **av)
{
	print_hex(atoi(av[1]));
}
