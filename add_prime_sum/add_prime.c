/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_prime.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 16:06:17 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 16:34:16 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int ft_atoi(char *str);

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void ft_putnbr(int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n < 10)
	{
		ft_putchar(n + 48);
		return ;
	}
	ft_putnbr(n / 10);
	ft_putchar(n % 10 + 48);
}

int is_prime(int n)
{
	int i = 0;

	if (n < 2)
		return (0);
	if (n == 2)
		return (1);
	if (n % 2 == 0)
		return (0);
	i = 3;
	while (i < n)
	{
		if (n % i == 0)
			return (0);
		i+=2;
	}
	return (1);
}

int add_prime(int n)
{
	int i;
	int res;

	i = 0;
	res = 0;
	while (i <= n)
	{
		if (is_prime(i) == 1)
			res += i;
		i++;
	}
	return (res);
}

int main(int ac, char **av)
{
	if (ac == 2)
		ft_putnbr(add_prime(ft_atoi(av[1])));
	ft_putchar('\n');
	return (0);
}

int ft_atoi(char *str)
{
	int i = 0;
	int n = 0;
	int res = 1;
	while(str[i] <= 32)
		i++;
	if (str[i] == '-')
	{
		res = -res;
		i++;
	}
	while(str[i] == '-' || str[i] == '+')
		i++;
	while(str[i] >= '0' && str[i] <= '9')
	{
		n = n * 10 + (str[i] - 48);
		i++;
	}
	return (n * res);
}
