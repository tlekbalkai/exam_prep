/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_b.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 20:27:53 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 20:31:38 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

void bits(unsigned char octet)
{
	int i = 0;
	int d = 128;
	int oct = octet;
	while (d != 0)
	{
		if (oct / d == 1)
		{
			printf("1");
			oct = oct % d;
		}
		else
		{
			printf("0");
		}
		d /= 2;
	}
}

int main(int ac, char **av)
{
	bits(atoi(av[1]));
	return (0);
}
