/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pgcd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 19:07:29 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 19:10:51 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int ft_min(int a, int b)
{
	if (a < b)
		return (a);
	else
		return (b);
}

int pgcd(int a, int b)
{
	int div = 1;
	int i = 1;
	while (i < ft_min(a, b))
	{
		if (a % i == 0 && b % i == 0)
			div = i;
		i++;
	}
	return (div);
}

int main(int ac, char **av)
{
	printf("%d\n", pgcd(atoi(av[1]), atoi(av[2])));
}
