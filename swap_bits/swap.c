/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 20:47:59 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 21:58:43 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
unsigned char swap_bits(unsigned char octet)
{
	return ((octet >> 4 & 0x0f) | (0xf0 & octet << 4));
}

int main(int ac, char **av)
{
	printf("%d\n", swap_bits(atoi(av[1])));
	return (0);
}
