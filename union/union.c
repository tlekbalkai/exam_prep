/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 15:36:26 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 15:51:28 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

int	verify(char c, char *str, int i)
{
	int j = 0;
	while (j < i)
	{
		if (str[j] == c)
			return (0);
		j++;
	}
	return (1);
}

void ft_union(char *str1, char *str2)
{
	int i = 0;
	int j;
	while (str1[i])
	{
		if (verify(str1[i], str1, i) == 1)
			ft_putchar(str1[i]);
		i++;
	}
	j = i;
	i = 0;
	while (str2[i])
	{
		if (verify(str2[i], str2, i) == 1)
		{
			if (verify(str2[i], str1, j) == 1)
				ft_putchar(str2[i]);
		}
		i++;
	}
}

int main(int ac, char **av)
{
	if (ac == 3)
		ft_union(av[1], av[2]);
	ft_putchar('\n');
	return (0);
}
