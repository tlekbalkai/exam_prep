/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 16:59:00 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 17:10:54 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int base_get(char c)
{
	int n;
	if (c >= '0' && c <= '9')
		n = c - 48;
	else if (c >= 'a' && c <= 'z')
		n = c - 'a' + 10;
	else if (c >= 'A' && c <= 'Z')
		n = c -'A' + 10;
	else
		n = -1;
	return (n);
}

int ft_atoi_base(char *str, int base)
{
	int res = 1;
	int num = 0;
	int i;
	int cur;

	while (str[i] <= 32)
		i++;
	if (str[i] == '-')
		res = -1;
	while (str[i] == '+' || str[i] == '+')
		i++;
	cur = base_get(str[i]);
	while (cur >= 0 && cur <= base)
	{
		num = num * base + cur;
		i++;
		cur = base_get(str[i]);
	}
	return (num * res);
}

int main(int ac, char **av)
{
	printf("%d\n", ft_atoi_base(av[1], atoi(av[2])));
}
