/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rot13.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 16:17:36 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 16:28:13 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	lower_c(char c)
{
	c = c - 'a';
	c = ((c + 13) % 26);
	c = c + 'a';
	ft_putchar(c);
}

void	upper_c(char c)
{
    c = c - 'A';
    c = ((c + 13) % 26);
    c = c + 'A';
    ft_putchar(c);
}

void	rot13(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			lower_c(str[i]);
		else if (str[i] >= 'A' && str[i] <= 'Z')
			upper_c(str[i]);
		else
			ft_putchar(str[i]);
		i++;
	}
}

int		main(int ac, char **av)
{
	if (ac == 2)
		rot13(av[1]);
	ft_putchar('\n');
	return (0);
}
