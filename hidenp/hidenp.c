/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hidenp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 19:12:45 by atlekbai          #+#    #+#             */
/*   Updated: 2018/03/01 19:18:53 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

void hidenp(char *str1, char *str2)
{
	int i = 0;
	int j = 0;
	while (str2[i] && str1[j])
	{
		if (str1[j] == str2[i])
			j++;
		i++;
	}
	if (str1[j] == '\0')
		printf("1");
	else
		printf("0");
}

int main(int ac, char **av)
{
	hidenp(av[1], av[2]);
}
