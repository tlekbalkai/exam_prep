/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   last.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 20:10:03 by atlekbai          #+#    #+#             */
/*   Updated: 2018/02/28 20:21:39 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

int ft_strlen(char *str)
{
	int i = 0;
	while (str[i])
		i++;
	return (i);
}

void last_word(char *str)
{
	int i;

	i = ft_strlen(str);
	while (str[i] <= 32)
		i--;
	while (str[i] > 32)
		i--;
	i++;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int main(int ac, char **av)
{
	if (ac == 2)
	{
		last_word(av[1]);
	}
	ft_putchar('\n');
	return (0);
}
